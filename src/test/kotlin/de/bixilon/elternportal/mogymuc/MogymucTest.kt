package de.bixilon.elternportal.mogymuc

import de.bixilon.elternportal.ElternPortal
import org.junit.jupiter.api.Assumptions.assumeTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MogymucTest {
    private lateinit var url: String
    private lateinit var username: String
    private lateinit var password: String
    private lateinit var elternPortal: ElternPortal

    private fun assume() {
        assumeTrue(this::elternPortal.isInitialized)
    }

    @BeforeAll
    @Test
    fun fetchEnvironment() {
        val url: String? = System.getenv("ELTERN_PORTAL_URL")
        val username: String? = System.getenv("ELTERN_PORTAL_USERNAME")
        val password: String? = System.getenv("ELTERN_PORTAL_PASSWORD")
        if (url == null || url.isBlank() || username == null || username.isBlank() || password == null || password.isBlank()) {
            System.err.println("url, username or password is empty, skipping mogymuc tests...")
            assumeTrue(false)
            return
        }
        this.url = url
        this.username = username
        this.password = password
    }

    @Order(1)
    @Test
    fun testLogin() {
        elternPortal = ElternPortal.createByURL(url)
        elternPortal.login(username, password)
    }

    @Test
    fun testSchoolInfo() {
        assume()
        val info = elternPortal.fetchSchoolInfo()
        assertEquals("gymnasium-muenchen-moosach@muenchen.de", info.email)
        assertEquals("233 83 100", info.phone)


        // nothing leaked here, names are visible on the website
        assertEquals("Stefan Illig", info.shortcodes["Il"])
        assertEquals("Cordula Endres", info.shortcodes["En"])
    }
}

package de.bixilon.elternportal

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals

internal class ElternPortalUtilTest {

    @Test
    fun testHTTPS() {
        assertEquals("https://mogymuc.eltern-portal.org", ElternPortalUtil.checkElternPortalURL("http://mogymuc.eltern-portal.org"))
    }

    @Test
    fun testWithoutProtocol() {
        assertEquals("https://mogymuc.eltern-portal.org", ElternPortalUtil.checkElternPortalURL("mogymuc.eltern-portal.org"))
    }

    @Test
    fun testSubdomain() {
        assertEquals("https://mogymuc.eltern-portal.org", ElternPortalUtil.checkElternPortalURL("mogymuc"))
    }

    @Test
    fun testRemovingSlash() {
        assertEquals("https://mogymuc.eltern-portal.org", ElternPortalUtil.checkElternPortalURL("mogymuc.eltern-portal.org/"))
    }

    @Test
    fun testInvalid1() {
        assertThrows<IllegalArgumentException> { ElternPortalUtil.checkElternPortalURL("") }
    }

    @Test
    fun testInvalid2() {
        assertThrows<IllegalArgumentException> { ElternPortalUtil.checkElternPortalURL("bixilon.de") }
    }

    @Test
    fun testInvalid3() {
        assertThrows<IllegalArgumentException> { ElternPortalUtil.checkElternPortalURL("mogymuc.eltern-portal.com") }
    }
}

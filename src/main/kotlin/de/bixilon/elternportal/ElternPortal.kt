/*
 * eltern-portal.org API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with art soft and more GmbH, the developer of eltern-portal.org
 */

package de.bixilon.elternportal

import de.bixilon.elternportal.login.InvalidCredentialsError
import de.bixilon.elternportal.login.LoginError
import de.bixilon.elternportal.login.UnknownLoginError
import de.bixilon.elternportal.service.SchoolInfo
import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.nodes.FormElement

class ElternPortal(
    val url: String,
    private var sessionKey: String,
    val schoolName: String,
    private val loginFormular: ElternPortalLoginFormular,
) {
    private val hostname = url.removePrefix("https://").removeSuffix("/")
    var state = ElternPortalStates.QUERIED
        private set

    private fun checkState() {
        check(state == ElternPortalStates.LOGGED_IN) { "Not logged in!" }
    }

    private fun fetch(url: String): Document {
        checkState()
        val response = Jsoup.connect("${this.url}$url")
            .method(Connection.Method.GET)
            .cookie(SESSION_COOKIE_NAME, sessionKey)
            .followRedirects(true)
            .execute()

        update(response)

        return response.parse()
    }


    private fun update(response: Connection.Response) {
        response.cookie(SESSION_COOKIE_NAME)?.let {
            sessionKey = it
        }

        // ToDo: Update login status
    }


    fun login(email: String, password: String) {
        val loginData = loginFormular.additionalFields.toMutableMap()

        loginData[loginFormular.emailFieldName] = email
        loginData[loginFormular.passwordFieldName] = password

        val response = Jsoup.connect("$url/includes/project/auth/login.php")
            .data(loginData)
            .method(Connection.Method.POST)
            .cookie(SESSION_COOKIE_NAME, sessionKey)
            .followRedirects(false)
            .execute()

        if (response.statusCode() != 302) {
            throw UnknownLoginError("Could not login (statusCode=${response.statusCode()})")
        }

        val redirect = response.header("Location") ?: throw LoginError("No Location header found!")

        if (redirect == "//$hostname/start" || hostname == "$url/start") {
            state = ElternPortalStates.LOGGED_IN
            update(response)
            return
        }

        val errorId = run {
            val data = redirect.split('?')

            for (entry in data) {
                val split = entry.split('=')
                val key = split[1]

                if (key != "errorno") {
                    continue
                }

                return@run split[2].toInt()
            }
            throw UnknownLoginError()
        }

        when (errorId) {
            1 -> throw InvalidCredentialsError(email = email)
            else -> TODO("Unknown error code: $errorId")
        }
    }


    fun logout() {
        fetch("/logout")
        sessionKey = ""
        state = ElternPortalStates.LOGGED_OUT
    }

    fun fetchSchoolInfo(): SchoolInfo {
        val document = fetch("/service/schulinformationen")

        val elements = document.getElementById("asam_content")!!.select("div[class=row m_bot]")

        var address: String? = null
        var phone: String? = null
        var fax: String? = null
        var email: String? = null
        var homepage: String? = null
        val shortcodes: MutableMap<String, String> = mutableMapOf()



        for (element in elements) {
            val property = element.getElementsByClass("hidden-sm")
            if (property.size != 1) {
                continue
            }
            val propertyName = property[0].text()

            val valueElements = element.getElementsByClass("col-md-6")
            if (valueElements.size != 1) {
                continue
            }
            val value = valueElements[0].text()


            when (propertyName) {
                "Adresse" -> address = value
                "Telefon" -> phone = value
                "Fax" -> fax = value
                "E-Mail" -> email = value
                "Homepage" -> homepage = value
                else -> {
                    if (propertyName.length in 1..5) { // Yah, shortcodes should be short
                        shortcodes[propertyName] = value
                    }
                }
            }
        }

        return SchoolInfo(
            address = address,
            phone = phone,
            fax = fax,
            email = email,
            homepage = homepage,
            shortcodes = shortcodes.toMap(),
        )
    }

    companion object {
        private const val SESSION_COOKIE_NAME = "PHPSESSID"

        fun getLoginFormularInfo(document: Document): ElternPortalLoginFormular {

            fun getLoginFormularElement(): FormElement {
                val possibleElements: MutableSet<Element> = mutableSetOf()

                document.getElementsByClass("form-signin").getOrNull(0)?.let {
                    possibleElements += it
                }
                document.getElementsByTag("form").let {
                    possibleElements += it
                }

                for (element in possibleElements) {
                    if (element is FormElement && element.formData().size >= 3) { // Minimum input fields: Email, password, csrf, (go_to)
                        return element
                    }
                    // ToDo: More checks?
                    continue
                }
                error("Can not find login formular. Is the URL correct?")
            }

            val formular = getLoginFormularElement()

            val actionURL = formular.attributes().get("action")

            var emailFieldName: String? = null
            var passwordFieldName: String? = null
            val additionalFields: MutableMap<String, String> = mutableMapOf()


            for (field in formular.formData()) {
                val key = field.key()

                if (emailFieldName == null && key.matches("(input)?([uU]sername|([eE][\\-]?mail))".toRegex())) {
                    emailFieldName = key
                    continue
                }
                if (passwordFieldName == null && key.matches("(input)?[pP]assword".toRegex())) {
                    passwordFieldName = key
                    continue
                }
                additionalFields[key] = field.value()
            }

            emailFieldName ?: error("Could not find email field!")
            passwordFieldName ?: error("Could not find password field!")

            return ElternPortalLoginFormular(
                actionURL = actionURL,
                emailFieldName = emailFieldName,
                passwordFieldName = passwordFieldName,
                additionalFields = additionalFields.toMap(),
            )
        }

        fun createByURL(url: String): ElternPortal {
            val realURL = ElternPortalUtil.checkElternPortalURL(url)
            val response = Jsoup.connect(realURL)
                .method(Connection.Method.GET)
                .execute()

            val sessionKey = response.cookie(SESSION_COOKIE_NAME) ?: error("Can not find session cookie!")

            val document = response.parse()
            val loginFormular = getLoginFormularInfo(document)

            val schoolName = document.getElementById("schule")?.text() ?: "Unknown school!"

            return ElternPortal(realURL, sessionKey, schoolName, loginFormular)
        }
    }
}

# Eltern Portal API

Out school (and many more in germany!) have an eltern-portal (parent's guide?). You can get various school information there, mark your self as sick, ... This is a pretty simple API used in a substitution plan app called [ESBDirect](https://notabug.org/fynngodau/DSBDirect). You can import the teachers short codes directly into the app.

## Requirements

- A `*.eltern-portal.org` account
- Java 8+
- Internet

## Usage

First add the library to your dependencies:

```xml
<dependency>
    <groupId>de.bixilon</groupId>
    <artifactId>eltern-portal.org</artifactId>
    <version>1.1</version>
</dependency>
```

(yes, sorry. I forgot the `-api` in the artifact id...)

Then follow the guide here:

```kotlin
package de.bixilon.elternportal

object ElternPortalTest {

    @JvmStatic
    fun main(args: Array<String>) {
        if (args.size != 3) {
            error("Usage: ./app <URL> <Email> <Password>")
        }

        // create instance with url
        val elternPortal = ElternPortal.createByURL(args[0])

        // login
        elternPortal.login(args[1], args[2])


        // E.g.: Fetch school info and use relevant information
        val schoolInfo = elternPortal.fetchSchoolInfo()
        println("If you want to contact your school, call ${schoolInfo.phone} :)")


        // Logout again
        elternPortal.logout()
    }
}
```
